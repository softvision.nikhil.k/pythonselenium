#Get a list of names from given file name.txt
import time
import logging

format_string = '%(levelname)s: %(asctime)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, filename="logger.log", format=format_string)


start = time.time()

# Read lines from file
with open("Names", 'r') as file_handle:
    lines = file_handle.read().splitlines()
string = '@pydemo.com'

# convert to lower case and concatination
my_list = [x.lower().replace(" ", "_")+string for x in lines]

# dictionary to remove duplicate values from the list
dup = list(dict.fromkeys(my_list))
print(dup)

# Write the output into file
f = open("Output", 'w')
l1 = map(lambda x: x+'\n', dup)
f.writelines(l1)
f.close()
print(time.time() - start)

logging.info("Assignment1")
logging.info(time.time() - start)
logging.info("Successfully executed")
