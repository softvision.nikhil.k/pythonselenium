import pytest
class Test_data:

        def test_CognizantOne(self):
                print("Cognizant1 is executed")

        @pytest.mark.smoke
        def test_credentials(self):
            print("enter username and password")

        @pytest.mark.smoke
        def test_Login(self):
                print("Successfully logged in")

        def test_CognizantTwo(self):
                print("Cognizant2 executed")

        def test_CognizantThree(self):
                print("Cognizant3 executed")

        @pytest.mark.skip
        def test_message_01(self):
                value = "Welcome"
                assert value == "error", "This failed because value did not match"

        @pytest.mark.xfail
        def test_invalid(self):
                print("test case fail")

        @pytest.mark.regression
        def test_dataload(self):
                print("Execution cf dataload completed")

        def test_register(self):
                print("Registration complete")

        def test_complete(self):
                print("End Execution")


